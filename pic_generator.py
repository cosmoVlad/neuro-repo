from original_pic_handler import OriginalPicHandler
from config import Params
from transformers import RealNoiseTransformer, FourierTransformer, FourierNoiseTransformer, Crop, SaveOnlyArcs,\
                        CenterTransformer, NormTransformer, AddChannelTransformer
from sklearn.pipeline import Pipeline
from sklearn.utils import resample
import numpy as np


class PicGeneratorArcs(OriginalPicHandler):
    def __init__(self):
        ## set self.data and self.label
        super().__init__(verbose=False)

    def set_pipe(self, min_filled_part, use_mask, use_phase, special_mode, add_borders):
        real_transformer_kwargs = Params.REAL_TRANSFORMER_KWARGS
        fourier_transformer_kwargs = Params.FOURIER_TRANSFORMER_KWARGS
        
        save_only_arcs_transformer_kwargs = Params.SAVE_ONLY_ARCS_TRANSFORMER_KWARGS
        save_only_arcs_transformer_kwargs['min_filled_part'] = min_filled_part
        save_only_arcs_transformer_kwargs['special_mode'] = special_mode
        save_only_arcs_transformer_kwargs['add_borders'] = add_borders

        self.pipe = Pipeline([
            ('real_noise', RealNoiseTransformer(**real_transformer_kwargs)),
            ('fourier', FourierTransformer()),
            ('fourier_noise', FourierNoiseTransformer(**fourier_transformer_kwargs)),
            ('center', CenterTransformer()),
            ('save_only_arcs', SaveOnlyArcs(**save_only_arcs_transformer_kwargs)),
            ('norm', NormTransformer()),
            ('AddChannelTransformer', AddChannelTransformer(use_mask, use_phase))
             ])

    def batch_generator(self, batch_size, blur_sigma, filled_part, use_mask, use_phase, special_mode, add_borders):
        self.set_pipe(filled_part, use_mask, use_phase, special_mode, add_borders)
        
        while True:
            X_train, y_train = resample(self.data, self.label, n_samples=batch_size)
            X_train_and_mask = (X_train, None, None)
            X_train_and_mask = self.pipe.transform(X_train_and_mask)
            
            yield X_train_and_mask, y_train

    def get_uncropped_picture(self, angle : "[0, pi/2.]", blur_sigma): #, rotate_angle=None, translate_px=None):
        self.set_pipe(blur_sigma, Params.MAX_BLOCKS)
        pics = self.pipe.transform(self.data)
        return pics[np.where(np.absolute(self.label - angle) == np.absolute(self.label - angle).min())]
