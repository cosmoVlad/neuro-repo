class Distance_Params:
    DISTANCE_TO_BH = 100.
    WIDTH_COEF = 2./3

class Params:
    PIC_FOLDER = "./eval_numeric/"
    
    USE_PHASE = False
    USE_MASK = True
    
    seed_models = "../models_{}/"
    seed_results = "./test_results_{}/"
    
    addendum = ''
    if USE_MASK:
        addendum += 'mask'
    if USE_PHASE:
        addendum += 'phase'
    if len(addendum) == 0:
        addendum += 'nothing'
        
    MODELS_FOLDER = seed_models.format(addendum)
    RESULTS_FOLDER = seed_results.format(addendum)
    
    #BLUR_SIGMA_VALUES = [0.1, 0.5, 0.9, 1.3, 1.7, 2.1, 2.5, 3.]
    BLUR_SIGMA_VALUES = [2.0]
    DROPOUTS = [0.]
    
    VISIBLE_BLOCKS_VALUES = [256]#, 150, 128, 90, 64, 32, 16, 8, 4, 2, 1]
    FILLED_PARTS = [-1]#, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    
    STEPS_PER_EPOCH = 100
    MODEL_CHECKPOINT_PERIOD = 25
    EPOCHS = 100
    FFT_S = 2
    INITIAL_SIZE = 128
    FINAL_SIZE = 64

    BATCH_SIZE = 64
    VAL_DATASET_SIZE = 2048
    NOISE_TRANSFORMER_KWARGS = {"translate_px" : (-15, 15), "rotate_angle" : (-180, 180), "blur_sigma" : 2.0}
    REAL_TRANSFORMER_KWARGS = {"rotate_angle" : (-180, 180)}
    FOURIER_TRANSFORMER_KWARGS = {"translate_px": {'x':(-15,15),'y':(-15,15)},\
       'blur_sigma': 5.2*INITIAL_SIZE/Distance_Params.DISTANCE_TO_BH/Distance_Params.WIDTH_COEF}
#     FOURIER_TRANSFORMER_KWARGS = {"translate_px": {'x':(-15,15),'y':(-15,15)},\
#     'blur_sigma': 10.}
    CROP_TRANSFORMER_KWARGS = {"mask_len" : 16, "block_len" : 8, "visible_blocks" : 256}
    SAVE_ONLY_ARCS_TRANSFORMER_KWARGS = {'min_filled_part':0.2, 'radius_params' : \
                                         {'is_radius_uniform' : True, 'uniform_min_radius':0, 'uniform_max_radius':FINAL_SIZE * 0.71\
                                         , 'gauss_mean_radius':40, 'gauss_std_radius':20, 'radius_delta':1}\
                                         , 'eccentricity_min':0.1, 'eccentricity_max':0.9\
                                         , 'arc_mean_length':3.1415926 * 2 / 3, 'arc_std_length' : 3.1415926 / 6}
    MAX_BLOCKS = 256
#    DISK_SIZE = 21

    
    CENTER_RADIUS_PART = 0.16
    BORDER_ARC_COUNT = 6
