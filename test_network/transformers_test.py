import numpy as np
from sklearn.base import TransformerMixin
from imgaug import augmenters as iaa
import random
from config_test import Params
import os

class RealNoiseTransformer(TransformerMixin):
    '''
    Noise transformer.
    Rotate
    '''
    def __init__(self, rotate_angle):
        super().__init__()
        self.rotate_angle = rotate_angle
        self._set_augmenter()
        
    def _set_augmenter(self):
        self.augmenter = iaa.Sequential([
            iaa.Affine(rotate=self.rotate_angle),                
            ], random_order = True)
        return

    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

        return (self.augmenter.augment_images(X_abs), X_angle, mask)
    
    def fit(self, X, y=None):
        return self

    
def pad_with(vector, pad_width, iaxis, kwargs):
    
    pad_value = kwargs.get('padder', 10)
    if iaxis == 1 or  iaxis ==2:
        vector[:pad_width[0]] = pad_value
        vector[-pad_width[1]:] = pad_value
    return vector
    
class FourierTransformer(TransformerMixin):
    '''
    Fourier transformer.
    Picture -> absolute(fft(picture)) for every picture in batch
    '''
    def __init__(self):
        super().__init__()
        
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

#         return np.absolute(np.fft.fft2(X, axes=(1,2)))
#         return np.absolute(np.fft.fftshift(np.fft.fft2(X, axes=(1,2))))

        X = X_abs

        k_pad = Params.FFT_S
        N = Params.INITIAL_SIZE
        M = k_pad*N
        
        ui,vi = np.meshgrid(np.arange(M),np.arange(M))
        
        result = None
        if k_pad == 0:
            result = np.fft.fftshift(np.fft.fftn(X, axes=(1,2)))
        else:
#             result = np.absolute(np.fft.fftshift(np.fft.fftn(X, axes=(1,2), s=(Params.FFT_S*128, Params.FFT_S*128)), axes=(1,2)))
            result = np.fft.fftshift(np.fft.fftn(X, axes=(1,2), s=(M, M)), axes=(1,2)).reshape(-1,M,M,)*\
                            np.exp(1j*np.pi*(N-1)*((ui+vi)/M - 1))


        return (np.absolute(result).reshape(-1,M,M,1), np.angle(result).reshape(-1,M,M,1), mask)
            
    def fit(self, X, y=None):
        return self
    
class FourierNoiseTransformer(TransformerMixin):
    '''
    Noise transformer in Fourier space.
    Blur: modify absolute(fft(image))
    Translate: modify angle(fft(image))
    '''
    def __init__(self, translate_px, blur_sigma):
        super().__init__()
        self.translate_px = translate_px
        self.blur_sigma = blur_sigma
        self._set_augmenters()
        
    def abs_blur(self,images, random_state, parents, hooks):
        k_pad = Params.FFT_S
        N = Params.INITIAL_SIZE
        M = k_pad*N
        num = images.shape[0]
        
        ui,vi = np.meshgrid(np.arange(M),np.arange(M))
        sigma = np.array([self.blur_sigma*random_state.rand(num)]*(M*M)).T.reshape(num,M,M)
        #print('transforming...')
        #sigma = np.array([np.full((num,),self.blur_sigma)]*(M*M)).T.reshape(num,M,M)
        final_images = np.exp(-2*(np.pi*sigma/M)**2*((ui-M/2)**2+(vi-M/2)**2))*images.reshape(num,M,M)
        
        return final_images.reshape(num,M,M,1)
    
    def angle_translate(self,images, random_state, parents, hooks):
        k_pad = Params.FFT_S
        N = Params.INITIAL_SIZE
        M = k_pad*N
        num = images.shape[0]
        
        ui,vi = np.meshgrid(np.arange(M),np.arange(M))
        dx = np.array([random_state.randint(*self.translate_px['x'], size=num)]*(M*M)).T.reshape(num,M,M)
        dy = np.array([random_state.randint(*self.translate_px['y'], size=num)]*(M*M)).T.reshape(num,M,M)
        final_images = 2*(np.pi/M)*(-dx*(ui-M/2) + dy*(vi-M/2)) + images.reshape(num,M,M)
        
        return final_images.reshape(num,M,M,1)
    
    def func_heatmaps(self,heatmaps, random_state, parents, hooks):
        return heatmaps
    
    def func_keypoints(self,keypoints_on_images, random_state, parents, hooks):
        return keypoints_on_images
    
        
    def _set_augmenters(self):
        self.augmenter_abs = iaa.Lambda(func_images=self.abs_blur,\
                                       func_heatmaps=self.func_heatmaps,\
                                       func_keypoints=self.func_keypoints)
        self.augmenter_angle = iaa.Lambda(func_images=self.angle_translate,\
                                       func_heatmaps=self.func_heatmaps,\
                                       func_keypoints=self.func_keypoints)
        return
    

    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

        return (self.augmenter_abs.augment_images(X_abs),\
                self.augmenter_angle.augment_images(X_angle), mask)
    
    def fit(self, X, y=None):
        return self
    
class InverseFourierTransformer(TransformerMixin):
    '''
    Inverse Fourier transformer.
    fft(picture) -> real(picture) for every picture in batch
    '''
    def __init__(self):
        super().__init__()
        
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

#         return np.absolute(np.fft.fft2(X, axes=(1,2)))
#         return np.absolute(np.fft.fftshift(np.fft.fft2(X, axes=(1,2))))


        k_pad = Params.FFT_S
        N = Params.INITIAL_SIZE
        M = k_pad*N
        
        ui,vi = np.meshgrid(np.arange(M),np.arange(M))
        
        angles = X_angle.reshape(-1,M,M,) - np.pi*(N-1)*((ui+vi)/M - 1)
        result_X = X_abs.reshape(-1,M,M,)*np.exp(1j*angles)
        result = np.fft.ifftn(np.fft.ifftshift(result_X, axes=(1,2)), axes=(1,2))[:,:N,:N]


        return (np.real(result).reshape(-1,N,N,1), np.imag(result).max(), mask)
            
    def fit(self, X, y=None):
        return self
    
    
class Crop(TransformerMixin):
    '''
    Picture -> cropped picture
    '''
    def __init__(self, mask_len, block_len, visible_blocks):
        super().__init__()
        self.mask_len = mask_len
        self.block_len = block_len
        self.visible_blocks = visible_blocks
    
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

        '''
        Tranform pic batch
        pixel_value -> pixel_value*mask_pixel_value
        '''
        mask = self.create_mask(self.mask_len, self.block_len, self.visible_blocks)
        result = np.multiply(mask, X.reshape(*X.shape[:-1]))
        result = result.reshape(*result.shape, 1)
        return (result, mask)
    
    def fit(self, X, y=None):
        return self

    def create_mask(self, mask_len, block_len, visible_blocks):
        '''
        Example:
        mask_len = 3
        block_len = 2
        visible_blocks = 3
        [
        [1, 1, 0, 0, 1, 1],
        [1, 1, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 0, 0, 0, 0],
        [1, 1, 0, 0, 0, 0],
        ]
        '''
        mask = np.zeros(mask_len*mask_len)
        if visible_blocks == -1:
            mask[:] = 1.
        else:        
            mask[np.random.choice(mask_len*mask_len, visible_blocks, replace=False)] = 1.
        mask = mask.reshape((mask_len, mask_len))
        mask = np.kron(mask, np.ones((block_len, block_len)))
        return mask

    

class SaveOnlyArcs(TransformerMixin):

    def __init__(self, min_filled_part, radius_params, eccentricity_min\
                 , eccentricity_max, arc_mean_length, arc_std_length, special_mode, add_borders, epoch, writable):
        super().__init__()
        self.min_filled_part = min_filled_part
        
        self.is_radius_uniform = radius_params.get('is_radius_uniform', True)
        self.uniform_min_radius = radius_params.get('uniform_min_radius', -1)
        self.uniform_max_radius = radius_params.get('uniform_max_radius', -1)
        self.gauss_mean_radius = radius_params.get('gauss_mean_radius', -1)
        self.gauss_std_radius = radius_params.get('gauss_std_radius', -1)
        self.radius_delta = radius_params.get('radius_delta', -1)
        
        self.special_mode = special_mode
        self.add_borders = add_borders
        self.epoch = epoch
        self.writable = writable

        self.eccentricity_min = eccentricity_min
        self.eccentricity_max = eccentricity_max

        self.arc_mean_length = arc_mean_length
        self.arc_std_length = arc_std_length
    
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

        X = X_abs
        
        mask_len = X.shape[1]
        batch_size = X.shape[0]
                
        mask = self.create_mask(mask_len, batch_size, writable=self.writable)
#         X[:,:,:] = 1

        X[mask == 0,:] = 0
        X_angle[mask == 0,:] = 0

    
        # TODO : deephack
        return (X, X_angle, mask)
    
    def fit(self, X, y=None):
        return self
    
    def norm_angle(self, arg_angle):
        angle = arg_angle
        temp = int(angle / 2 / np.pi)
        angle = angle - temp * 2 * np.pi
        if angle > np.pi:
            angle -= 2 * np.pi
        return angle
    
    def is_angle_in_range(self, angle, arg_angle_min, arg_angle_max):
        angle_min = min(arg_angle_min, arg_angle_max)
        angle_max = max(arg_angle_min, arg_angle_max)
        
        if angle_max - angle_min < np.pi:
            return np.logical_and(angle_min < angle, angle < angle_max)
        else:
            return np.logical_or(angle_min > angle, angle_max < angle)
            
    
    def add_arc(self, mask_len, x0, y0, alpha_min, alpha_max, a_min, a_max, b_min, b_max):
        size = mask_len
        
        mask = np.zeros((mask_len, mask_len))
        
        nx, ny = (size, size)
        x = np.linspace(0, size + 1, nx)
        y = np.linspace(0, size + 1, ny)
        xv, yv = np.meshgrid(x, y)

        tan_per_point = np.arctan2(((yv-y0) / b_min), ((xv-x0) / a_min))
        
        in_ellipse = np.logical_and(((xv-x0) / a_min)**2 + ((yv-y0) / b_min)**2 > 1 \
                                , ((xv-x0) / a_max)**2 + ((y0-yv) / b_max)**2 < 1)
        
        in_angle = self.is_angle_in_range(tan_per_point, alpha_min, alpha_max)
                        
        return np.logical_and(in_ellipse, in_angle)
    
    
    def add_two_sym_arcs(self, mask_len, x0, y0, alpha_min, alpha_max, a_min, a_max, b_min, b_max, mask):
        mask[self.add_arc(mask_len, x0, y0, alpha_min, alpha_max, a_min, a_max, b_min, b_max)] = 1
        mask[self.add_arc(mask_len, mask_len - x0, mask_len - y0\
                          , self.norm_angle(np.pi + alpha_min),\
                          self.norm_angle(np.pi + alpha_max), a_min, a_max, b_min, b_max)] = 1
    
        return mask
    
    def generate_radius_mean(self, is_center=False):
        if self.is_radius_uniform:
            if self.special_mode:
                if is_center:
                    radius_mean_min = self.uniform_min_radius
                    radius_mean_max = self.uniform_max_radius * Params.CENTER_RADIUS_PART
                    radius_mean = np.random.uniform(radius_mean_min, radius_mean_max, 1)[0]
                else:
                    radius_mean = np.random.normal(Params.FINAL_SIZE / 2, Params.FINAL_SIZE / 100, 1)[0]
            else:
                radius_mean_min = self.uniform_min_radius
                radius_mean_max = self.uniform_max_radius
                radius_mean = np.random.uniform(radius_mean_min, radius_mean_max, 1)[0]
        else:
            radius_mean = np.random.normal(self.gauss_mean_radius, self.gauss_std_radius, 1)[0]
        return radius_mean
        
    
    def create_mask_part(self, mask_len, batch_size, masks, is_center=False):
#         masks = np.zeros((batch_size, mask_len, mask_len))
        
        full_square = mask_len**2
        if self.special_mode:
            full_square *= Params.CENTER_RADIUS_PART**2
        
        for image_i in range(batch_size):

            mask = np.zeros((mask_len, mask_len))
        
            required_filled_part = self.min_filled_part
            if self.min_filled_part == -1:
                required_filled_part = np.random.uniform(0.1, 0.9, 1)[0]

            arc_i = 0
            while True:
                if self.special_mode and not is_center:
                    if arc_i > Params.BORDER_ARC_COUNT:
                        break
                elif mask.sum() / full_square > required_filled_part:
                    break
    #             print(filled_part)

                border = 10
                center_x = 0.5 * mask_len #np.random.uniform(0.5, 1, 1)[0] * (mask_len - border);
                center_y = 0.5 * mask_len #np.random.uniform(0, 1, 1)[0] * (mask_len - border);

                radius_mean = self.generate_radius_mean(is_center)
                
                radius_min = radius_mean - self.radius_delta / 2
                radius_max = radius_mean + self.radius_delta / 2

    #             print(radius_min, radius_max)
                eccentricity = np.random.uniform(self.eccentricity_min, self.eccentricity_max, 1)[0]

                orientation = random.getrandbits(1)
                mult_x = 1 / np.power(1 - eccentricity**2, 0.25)
                mult_y = np.power(1 - eccentricity**2, 0.25)

                if orientation == 1:
                    mult_x, mult_y = mult_y, mult_x

                a_min = radius_min * mult_x
                a_max = radius_max * mult_x

                b_min = radius_min * mult_y
                b_max = radius_max * mult_y

                arc_length = np.random.normal(self.arc_mean_length, self.arc_std_length, 1)[0]

                arc_min_angle = np.random.uniform(-0.5, 0, 1)[0] * np.pi
                arc_max_angle = arc_min_angle + arc_length

                mask = self.add_two_sym_arcs(mask_len, center_x, center_y, arc_min_angle, arc_max_angle, a_min, a_max, b_min, b_max, mask)
    #         mask = self.add_two_sym_arcs(mask_len, 3 * mask_len / 5 - 10, 2 * mask_len / 5 - 10, -np.pi/3, 1 * np.pi/4, 15, 18, 5, 8, mask)
#             print(image_i, filled_part)
                arc_i += 1
            masks[image_i,:] = np.minimum(1, masks[image_i,:] + mask)
        return masks

    def create_mask(self, mask_len, batch_size, writable=False):
#         masks = np.zeros((batch_size, mask_len, mask_len))
#         if self.add_borders:
#             masks = self.create_mask_part(mask_len, batch_size, masks, is_center=False)
#         masks = self.create_mask_part(mask_len, batch_size, masks, is_center=True)

#         return masks
    
        masks = np.zeros((batch_size, mask_len, mask_len))
        masks = self.create_mask_part(mask_len, batch_size, masks, is_center=True)
#         if self.add_borders:
#             masks = self.create_mask_part(mask_len, batch_size, masks, is_center=False)
        
        full_square = mask_len**2
#         if self.special_mode:
#             full_square *= Params.CENTER_RADIUS_PART**2
        average_filled_part = np.average(np.sum(masks, axis=(1,2))/full_square)
        print('Average filled_part: ', average_filled_part)
        #print('Epoch:', self.epoch)
        
        folder_path = Params.RESULTS_FOLDER
        filename_schema = os.path.join(folder_path, \
                            "filled_parts_epoch_{}_sp_{}_borders_{}.txt")
        filename = filename_schema.format(self.epoch, self.special_mode, self.add_borders)
        
        if writable:
            with open(filename, 'a+') as f:
                f.write('{:.4f}'.format(average_filled_part) + '\n')
        
        if self.add_borders:
            masks = self.create_mask_part(mask_len, batch_size, masks, is_center=False)
        #masks = self.create_mask_part(mask_len, batch_size, masks, is_center=True)
        
        return masks
    
class CenterTransformer(TransformerMixin):
    def __init__(self):
        super().__init__()
        
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

        X = X_abs
        one_dim_size = X.shape[1]
        step = int((one_dim_size - Params.FINAL_SIZE) / 2)
#         return np.absolute(np.fft.fftshift(np.fft.fft2(X, axes=(1,2))))
#         return np.absolute(np.fft.fftshift(np.fft.fft2(X, axes=(1,2))))

        temp_abs = X[:, step:one_dim_size-step, step:one_dim_size-step, :]
        temp_angle = X_angle[:, step:one_dim_size-step, step:one_dim_size-step, :]
    
        return (temp_abs, temp_angle, mask)

    
    def fit(self, X, y=None):
        return self
    

class NormTransformer(TransformerMixin):
    def __init__(self):
        super().__init__()
        
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask

        X = X_abs
        one_dim_size = X.shape[1]

        max_val = np.max(X, axis=(1,2, 3))
        length = max_val.shape[0]
        max_val = max_val.reshape(length, 1, 1, 1)
        
        # TODO : make another transformer for rotate angle
        #add_phase = np.random.uniform(low=0.0, high=2 * np.pi, size=X.shape[0])
        #add_phase = add_phase.reshape(X.shape[0], 1, 1, 1)
        #X_angle = X_angle + add_phase
        #X_angle = (X_angle - 2 * np.pi) * (X_angle > 2 *np.pi) + X_angle*(X_angle < 2 *np.pi)
        
        return (X / max_val, X_angle, mask)

    
    def fit(self, X, y=None):
        return self
    

class AddChannelTransformer(TransformerMixin):
    def __init__(self, use_mask, use_phase):
        super().__init__()
        self.use_mask = use_mask
        self.use_phase = use_phase
        
    def transform(self, abs_angle_and_mask, y=None):
        X_abs, X_angle, mask = abs_angle_and_mask
        
        mask = np.expand_dims(mask, axis=-1)
        
        temp = X_abs
        if self.use_phase:
            temp = np.concatenate((temp, np.sin(X_angle), np.cos(X_angle)), axis=3)
        
        if self.use_mask:
            temp = np.concatenate((temp, mask), axis=3)

        return temp
    
    def fit(self, X, y=None):
        return self
    
