from tensorflow.keras.models import Sequential
from tensorflow.keras.losses import mean_absolute_error, mean_squared_error
from tensorflow.keras.optimizers import Adam, Nadam, RMSprop
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D, Activation, BatchNormalization
#import keras
from tensorflow.keras.applications import VGG16
from tensorflow.keras.layers import *
from config_test import Params

import tensorflow.keras as keras


class Gray2VGGInput( Layer ) :
    """Custom conversion layer
    """
    def build( self, x ) :
        self.image_mean = K.variable(value=np.array([103.939, 116.779, 123.68]).reshape([1,1,3]).astype('float32'), 
                                     dtype='float32', 
                                     name='imageNet_mean' )
        self.built = True
        return
    def call( self, x ) :
        rgb_x = K.concatenate( [x,x,x], axis=-1 )
        norm_x = rgb_x - self.image_mean
        return norm_x
    def compute_output_shape( self, input_shape ) :
        return input_shape[:2] + (3,)

def create_model(use_mask, use_phase, dropout=0.5):
    print('dropout = %.1f' % (dropout))
    channels_count = 1 + int(use_mask)
    if use_phase:
        channels_count += 2

    model = Sequential()
    model.add(Conv2D(16, kernel_size=(3, 3), activation=None, input_shape=(Params.FINAL_SIZE, Params.FINAL_SIZE, channels_count)))
    
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(16, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    model.add(Dropout(dropout))
    model.add(Dense(16, activation='relu', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    model.add(Dropout(dropout))
    model.add(Dense(1, activation='linear', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
#     model.compile(loss=mean_absolute_error, optimizer=Adam(), metrics=['mae'])
    model.compile(loss=mean_squared_error, optimizer=Adam(), metrics=['mse', 'mae'])

    print('params',model.count_params())

    return model
    
#     gray_image = Input( shape=(Params.FINAL_SIZE,Params.FINAL_SIZE,1), name='gray_input' )
    # 3. convert to VGG input
#     vgg_input_image = Gray2VGGInput( name='gray_to_rgb_norm', input_shape=(Params.FINAL_SIZE, Params.FINAL_SIZE, 1))#( gray_image )
    
#     conv_base = VGG16(weights='imagenet',
#                   include_top=False,
#                   input_shape=(128, 128, 3))
    
#     model = Sequential()

#     model.add(vgg_input_image)
    
#     model.add(conv_base)
#     model.add(Flatten())
#     model.add(Dense(256, activation='relu'))
#     model.add(Dense(1, activation='sigmoid'))

#     conv_base.trainable = False
#     model.compile(loss=mean_squared_error, optimizer=RMSprop(lr=2e-5), metrics=['mse', 'mae'])

#     return model
    
    
    model = Sequential()
    
    model.add(Reshape((Params.FINAL_SIZE*Params.FINAL_SIZE, 1), input_shape=(Params.FINAL_SIZE, Params.FINAL_SIZE, 1)))
#     model.add(Conv2D(1, kernel_size=(Params.FINAL_SIZE, Params.FINAL_SIZE),
#                      activation='relu',
#                      input_shape=(Params.FINAL_SIZE, Params.FINAL_SIZE, 1)))
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(Dropout(0.5))
    
    model.add(Dense(4, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='linear', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    
    model.compile(loss=mean_absolute_error, optimizer=Adam(), metrics=['mse', 'mae'])

    print('params',model.count_params())
    return model

    
    
#     model = Sequential()
#     model.add(Conv2D(32, kernel_size=(3, 3),
#                      activation='relu',
#                      input_shape=(Params.FINAL_SIZE, Params.FINAL_SIZE, 1)))
#     model.add(Conv2D(64, (3, 3), activation='relu'))
#     model.add(MaxPooling2D(pool_size=(2, 2)))
#     model.add(Dropout(0.25))
#     model.add(Flatten())
#     model.add(Dense(128, activation='relu'))
#     model.add(Dropout(0.5))
#     model.add(Dense(1, activation='linear', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    
#     model.compile(loss=mean_absolute_error, optimizer=RMSprop(), metrics=['mse', 'mae'])

#     print('params',model.count_params())
#     return model


    
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3), activation=None, input_shape=(Params.FINAL_SIZE, Params.FINAL_SIZE, 1)))
    model.add(Activation('relu'))
#     model.add(MaxPooling2D(pool_size=(2,2)))
#     model.add(Conv2D(16, (3, 3), activation='relu'))
#     model.add(MaxPooling2D(pool_size=(2,2)))
#     model.add(Conv2D(32, (3, 3), activation='relu'))
#     model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    model.add(Dense(32, activation='relu', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    
#     model.add(BatchNormalization())
    model.add(Dropout(0.5))
    model.add(Dense(16, activation='relu', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='linear', kernel_initializer=keras.initializers.he_normal(seed=None), bias_initializer='zeros'))
    
    model.compile(loss=mean_absolute_error, optimizer=Adam(), metrics=['mse', 'mae'])
#     model.compile(loss=mean_absolute_error, optimizer=RMSprop(), metrics=['mse', 'mae'])

    print('params',model.count_params())
    return model
