import numpy as np
import os
import re
from glob import iglob
from config_test import Params

class OriginalPicHandler:
    '''
    Load and handle data + labels
    '''
    def __init__(self, verbose=True):
        self.verbose = verbose
        self._pic_folder = Params.PIC_FOLDER
        #self._disk_size = Params.DISK_SIZE   # must use all sizes
        self._glob_regex = os.path.join(self._pic_folder, '*')
        #self._shadow_regex = r'disk_ang(\d+)_size{}.txt'.format(self._disk_size) #r'shadow(\d+)\.txt'
        self.regnum = '[0-9]*[.]?[0-9]+'
        self._shadow_regex = r'disk_ang=({0})_size={0}_rot={0}_val=[+-]1.gz'.format(self.regnum)
        self._filenames =  [f for f in iglob(self._glob_regex) if re.findall(self._shadow_regex, f)]
        self.data, self.label = self._load()
        print("Data shape : {} , label shape : {}".format(self.data.shape, self.label.shape))

    def _load(self):
        pics, angles = [], []
        for filename in self._filenames:
            if self.verbose:
                print("Add {} picture".format(filename))
            pic, angle = self._load_picture(filename)
            
#             if angle != 50:
#                 continue
            
            pics.append(pic)
            angles.append(angle)

        angles = np.array(angles)/90.
        pics = np.array(pics)
        
#         print(angles)
        
        pics = pics.reshape(*pics.shape, 1)
        return pics, angles
    
    def _load_picture(self, filename):
#         with open(filename, 'r') as f:
#             pic = np.array([row.split(' ') for row in f.read().split('\n') if row], dtype=float)
#             angle = re.findall(self._shadow_regex, filename)[0]
#             angle = np.float(angle)
        pic = np.loadtxt(filename)
        angle = re.findall(self._shadow_regex, filename)[0]
        angle = np.float(angle)
        return pic, angle
    
    def get_original_batch(self, batch_size): 
        indexes = np.random.choice(len(self.data), batch_size)
        return self.data[indexes], self.label[indexes]
