from config import Params
from model_creator import create_model
from pic_generator import PicGeneratorArcs
from glob import iglob
import os
from numpy.random import seed

from tensorflow.keras.callbacks import ModelCheckpoint, Callback, History
import shutil
import json
#from clr_callback import *
import time

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

os.environ['TFF_CPP_MIN_LOG_LEVEL'] = '2'
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

class TimeHistory(Callback):
    def on_train_begin(self, logs={}):
        self.begin_time = time.time()

    def on_train_end(self, logs={}):
        self.total_time = time.time() - self.begin_time

    
class ModelTrainer_Arcs(PicGeneratorArcs):
    def run(self):
        if (os.path.exists(Params.MODELS_FOLDER)):
            shutil.rmtree(Params.MODELS_FOLDER)
        os.mkdir(Params.MODELS_FOLDER)
        
        use_mask = Params.USE_MASK
        use_phase = Params.USE_PHASE
        
        for special_mode in [True,False]:
            borders_modes = [True, False]
            if not special_mode: borders_modes = [False]
            for add_borders in borders_modes:

                for blur_sigma, filled_part, dropout in self.param_generator():
                    self.run_network(blur_sigma, filled_part, dropout, use_mask, use_phase, special_mode, add_borders)

    def run_network(self, blur_sigma, filled_part, dropout, use_mask, use_phase, special_mode, add_borders):
        print("Run mask={} phase={} dropout={}, blur={}, filled parts={} special_mode={} add_borders={} network"\
              .format(use_mask, use_phase, dropout, blur_sigma, filled_part, special_mode, add_borders))
        seed(1)
        model = create_model(use_mask, use_phase, dropout)
        train_generator = self.batch_generator(Params.BATCH_SIZE, blur_sigma\
                                               , filled_part, use_mask, use_phase, special_mode, add_borders)
        x_valid, y_valid = next(self.batch_generator(Params.VAL_DATASET_SIZE, blur_sigma\
                                                     , filled_part, use_mask, use_phase, special_mode, add_borders))

        
        file_schema = self.params_to_file_schema(blur_sigma, dropout, special_mode, add_borders)
        callback = ModelCheckpoint(file_schema, monitor='val_loss', verbose=0, save_best_only=False, 
                                    save_weights_only=False, mode='auto',
                                    period=Params.MODEL_CHECKPOINT_PERIOD
                    )

        #history = History()
        time_history = TimeHistory()
        history = model.fit(train_generator, steps_per_epoch=Params.STEPS_PER_EPOCH, epochs=Params.EPOCHS,
                            callbacks = [time_history, callback],
                            validation_data=(x_valid, y_valid), verbose=1,
        )
        
        with open('history mask={} phase={} dropout={} special_mode={} add_borders={}.txt'.\
                  format(use_mask, use_phase, dropout, special_mode, add_borders), 'wt') as history_file:
            history_file.write(json.dumps(history.history))
        print('Total_time = %.2f s' % time_history.total_time)
        return

    def param_generator(self):
        for blur_sigma in Params.BLUR_SIGMA_VALUES:
            for filled_part in Params.FILLED_PARTS:
                for dropout in Params.DROPOUTS:
                    yield (blur_sigma, filled_part, dropout)

    def model_created(self, blur_sigma, filled_part):
        sub = 'blur_{}_'.format(blur_sigma)
        for filename in iglob(os.path.join(Params.MODELS_FOLDER, '*')):
            if sub in filename:
                return True
        return False

    def params_to_file_schema(self, blur_sigma, dropout, special_mode, add_borders):
        sub = 'blur_{}_dropout_{}_special_mode_{}_add_borders_{}_'.format(blur_sigma, dropout, special_mode, add_borders)
        subschema = sub + 'weights.{epoch:02d}-{val_loss:.4f}.hdf5'
        return os.path.join(Params.MODELS_FOLDER, subschema)


if __name__ == '__main__':
    ModelTrainer_Arcs().run()

